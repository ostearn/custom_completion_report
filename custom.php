<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course completion progress report
 *
 * @package    report
 * @subpackage completion
 * @copyright  2009 Catalyst IT Ltd
 * @author     Aaron Barnes <aaronb@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__).'/../../config.php');
require_once("{$CFG->libdir}/completionlib.php");

/**
 * Configuration
 */
define('COMPLETION_REPORT_PAGE',        25);
define('COMPLETION_REPORT_COL_TITLES',  true);

//****define an array of your courses (id => name to display)

$courses_to_include=array("2"=>"Sesion de Grupo 1","3"=>"Sesion de Grupo 2","15"=>"Tarea Familiar #1","8"=>"Sesion de Grupo 3","9"=>"Sesion de Grupo 4","16"=>"Tarea Familiar #2","10"=>"Sesion de Grupo 5","17"=>"Tarea Familiar #3","11"=>"Sesion de Grupo 6","12"=>"Sesion de Grupo 7","18"=>"Tarea Familiar #4", "13"=>"Sesion de Grupo 8");

$course_totals = array_fill_keys(array_keys($courses_to_include), 0);



/*
 * Setup page, check permissions
 */

// Get course
$courseid = required_param('course', PARAM_INT);
$format = optional_param('format','',PARAM_ALPHA);
$sort = optional_param('sort','',PARAM_ALPHA);
$edituser = optional_param('edituser', 0, PARAM_INT);


$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = new moodle_url('/report/completion/custom.php', array('course'=>$course->id));
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');

$firstnamesort = ($sort == 'firstname');
$excel = ($format == 'excelcsv');
$csv = ($format == 'csv' || $excel);

// Load CSV library
if ($csv) {
    require_once("{$CFG->libdir}/csvlib.class.php");
}

// Paging
$start   = optional_param('start', 0, PARAM_INT);
$sifirst = optional_param('sifirst', 'all', PARAM_NOTAGS);
$silast  = optional_param('silast', 'all', PARAM_NOTAGS);

// Whether to show extra user identity information
$extrafields = get_extra_user_fields($context);
$leftcols = 1 + count($extrafields);

// Check permissions
require_login($course);

require_capability('report/completion:view', $context);

// Get group mode
$group = groups_get_course_group($course, true); // Supposed to verify group
if ($group === 0 && $course->groupmode == SEPARATEGROUPS) {
    require_capability('moodle/site:accessallgroups',$context);
}

/**
 * Load data
 */

//****everything seems ok to this point

// Retrieve course_module data for all modules in the course
$modinfo = get_fast_modinfo($course);

// Get criteria for course
$completion = new completion_info($course);

//****
//print_r($completion);
//completion_info Object ( [course:completion_info:private] => stdClass Object ( [id] => 6 [category] => 1 [sortorder] => 10001 [fullname] => Test Course [shortname] => Test Course [idnumber] => [summary] => [summaryformat] => 1 [format] => topics [showgrades] => 1 [newsitems] => 5 [startdate] => 1496444400 [marker] => 0 [maxbytes] => 0 [legacyfiles] => 0 [showreports] => 0 [visible] => 1 [visibleold] => 1 [groupmode] => 1 [groupmodeforce] => 0 [defaultgroupingid] => 0 [lang] => [calendartype] => [theme] => [timecreated] => 1496419593 [timemodified] => 1500409209 [requested] => 0 [enablecompletion] => 1 [completionnotify] => 0 [cacherev] => 1500409209 ) [course_id] => 6 [criteria:completion_info:private] => )


if (!$completion->has_criteria()) {
    print_error('nocriteriaset', 'completion', $CFG->wwwroot.'/course/report.php?id='.$course->id);
}

// Get criteria and put in correct order
$criteria = array();

foreach ($completion->get_criteria(COMPLETION_CRITERIA_TYPE_COURSE) as $criterion) {
    $criteria[] = $criterion;
}

foreach ($completion->get_criteria(COMPLETION_CRITERIA_TYPE_ACTIVITY) as $criterion) {
    $criteria[] = $criterion;
}

foreach ($completion->get_criteria() as $criterion) {
    if (!in_array($criterion->criteriatype, array(
            COMPLETION_CRITERIA_TYPE_COURSE, COMPLETION_CRITERIA_TYPE_ACTIVITY))) {
        $criteria[] = $criterion;
    }
}

// Can logged in user mark users as complete?
// (if the logged in user has a role defined in the role criteria)
$allow_marking = false;
$allow_marking_criteria = null;

if (!$csv) {
    // Get role criteria
    $rcriteria = $completion->get_criteria(COMPLETION_CRITERIA_TYPE_ROLE);

    if (!empty($rcriteria)) {

        foreach ($rcriteria as $rcriterion) {
            $users = get_role_users($rcriterion->role, $context, true);

            // If logged in user has this role, allow marking complete
            if ($users && in_array($USER->id, array_keys($users))) {
                $allow_marking = true;
                $allow_marking_criteria = $rcriterion->id;
                break;
            }
        }
    }
}

/*
 * Setup page header
 */
if ($csv) {

    //$shortname = format_string($course->shortname, true, array('context' => $context));
    //$shortname = preg_replace('/[^a-z0-9-]/', '_',core_text::strtolower(strip_tags($shortname)));
    //$shortname = "Course Completion Report";

    $export = new csv_export_writer();
    $export->set_filename('Course Completion Report');

} else {
    // Navigation and header
    $strcompletion = get_string('coursecompletion');

    $PAGE->set_title($strcompletion);
    $PAGE->set_heading($course->fullname);

    echo $OUTPUT->header();

    $PAGE->requires->js('/report/completion/textrotate.js');
    $PAGE->requires->js_function_call('textrotate_init', null, true);

    // Handle groups (if enabled)
    groups_print_course_menu($course, $CFG->wwwroot.'/report/completion/custom.php?course='.$course->id);
}

// Generate where clause
$where = array();
$where_params = array();

if ($sifirst !== 'all') {
    $where[] = $DB->sql_like('u.firstname', ':sifirst', false);
    $where_params['sifirst'] = $sifirst.'%';
}

if ($silast !== 'all') {
    $where[] = $DB->sql_like('u.lastname', ':silast', false);
    $where_params['silast'] = $silast.'%';
}

// Get user match count
$total = $completion->get_num_tracked_users(implode(' AND ', $where), $where_params, $group);

// Total user count
$grandtotal = $completion->get_num_tracked_users('', array(), $group);

// If no users in this course what-so-ever
if (!$grandtotal) {
    echo $OUTPUT->container(get_string('err_nousers', 'completion'), 'errorbox errorboxcontent');
    echo $OUTPUT->footer();
    exit;
}

// Get user data
$progress = array();

if ($total) {
    $progress = $completion->get_progress_all(
        implode(' AND ', $where),
        $where_params,
        $group,
        $firstnamesort ? 'u.firstname ASC' : 'u.lastname ASC',
        $csv ? 0 : COMPLETION_REPORT_PAGE,
        $csv ? 0 : $start,
        $context
    );
}

//****print_r($progress);

//Array ( [3] => stdClass Object ( [id] => 3 [idnumber] => [firstnamephonetic] => [lastnamephonetic] => [middlename] => [alternatename] => [firstname] => student [lastname] => 1 [email] => test@fakeemail.com [progress] => Array ( ) ) [4] => stdClass Object ( [id] => 4 [idnumber] => [firstnamephonetic] => [lastnamephonetic] => [middlename] => [alternatename] => [firstname] => student [lastname] => 2 [email] => test@oliverstearn.com [progress] => Array ( [103] => stdClass Object ( [id] => 3 [coursemoduleid] => 103 [userid] => 4 [completionstate] => 1 [viewed] => 1 [timemodified] => 1497898678 ) ) ) [5] => stdClass Object ( [id] => 5 [idnumber] => [firstnamephonetic] => [lastnamephonetic] => [middlename] => [alternatename] => [firstname] => student [lastname] => 3 [email] => this@oliverstearn.com [progress] => Array ( ) ) [8] => stdClass Object ( [id] => 8 [idnumber] => [firstnamephonetic] => [lastnamephonetic] => [middlename] => [alternatename] => [firstname] => Oliver [lastname] => Test [email] => oliver@oliverstearn.com [progress] => Array ( [103] => stdClass Object ( [id] => 2 [coursemoduleid] => 103 [userid] => 8 [completionstate] => 1 [viewed] => 1 [timemodified] => 1497110131 ) ) ) ) 



// Build link for paging
$link = $CFG->wwwroot.'/report/completion/custom.php?course='.$course->id;
if (strlen($sort)) {
    $link .= '&amp;sort='.$sort;
}
$link .= '&amp;start=';

// Build the the page by Initial bar
$initials = array('first', 'last');
$alphabet = explode(',', get_string('alphabet', 'langconfig'));

$pagingbar = '';
foreach ($initials as $initial) {
    $var = 'si'.$initial;

    $othervar = $initial == 'first' ? 'silast' : 'sifirst';
    $othervar = $$othervar != 'all' ? "&amp;{$othervar}={$$othervar}" : '';

    $pagingbar .= ' <div class="initialbar '.$initial.'initial">';
    $pagingbar .= get_string($initial.'name').':&nbsp;';

    if ($$var == 'all') {
        $pagingbar .= '<strong>'.get_string('all').'</strong> ';
    }
    else {
        $pagingbar .= "<a href=\"{$link}{$othervar}\">".get_string('all').'</a> ';
    }

    foreach ($alphabet as $letter) {
        if ($$var === $letter) {
            $pagingbar .= '<strong>'.$letter.'</strong> ';
        }
        else {
            $pagingbar .= "<a href=\"$link&amp;$var={$letter}{$othervar}\">$letter</a> ";
        }
    }

    $pagingbar .= '</div>';
}

// Do we need a paging bar?
if ($total > COMPLETION_REPORT_PAGE) {

    // Paging bar
    $pagingbar .= '<div class="paging">';
    $pagingbar .= get_string('page').': ';

    $sistrings = array();
    if ($sifirst != 'all') {
        $sistrings[] =  "sifirst={$sifirst}";
    }
    if ($silast != 'all') {
        $sistrings[] =  "silast={$silast}";
    }
    $sistring = !empty($sistrings) ? '&amp;'.implode('&amp;', $sistrings) : '';

    // Display previous link
    if ($start > 0) {
        $pstart = max($start - COMPLETION_REPORT_PAGE, 0);
        $pagingbar .= "(<a class=\"previous\" href=\"{$link}{$pstart}{$sistring}\">".get_string('previous').'</a>)&nbsp;';
    }

    // Create page links
    $curstart = 0;
    $curpage = 0;
    while ($curstart < $total) {
        $curpage++;

        if ($curstart == $start) {
            $pagingbar .= '&nbsp;'.$curpage.'&nbsp;';
        }
        else {
            $pagingbar .= "&nbsp;<a href=\"{$link}{$curstart}{$sistring}\">$curpage</a>&nbsp;";
        }

        $curstart += COMPLETION_REPORT_PAGE;
    }

    // Display next link
    $nstart = $start + COMPLETION_REPORT_PAGE;
    if ($nstart < $total) {
        $pagingbar .= "&nbsp;(<a class=\"next\" href=\"{$link}{$nstart}{$sistring}\">".get_string('next').'</a>)';
    }

    $pagingbar .= '</div>';
}

/*
 * Draw table header
 */

// Start of table
if (!$csv) {
    print '<br class="clearer"/>'; // ugh

    $total_header = ($total == $grandtotal) ? $total : "{$total}/{$grandtotal}";
    echo $OUTPUT->heading(get_string('allparticipants').": {$total_header}", 3);

    print $pagingbar;

    if (!$total) {
        echo $OUTPUT->heading(get_string('nothingtodisplay'), 2);
        echo $OUTPUT->footer();
        exit;
    }

    print '<table id="completion-progress" class="generaltable flexible boxaligncenter completionreport" style="text-align: left" cellpadding="5" border="1">';

    // Print criteria group names
    print PHP_EOL.'<thead><tr style="vertical-align: top">';
    echo '<th scope="row" class="rowheader" colspan="' . $leftcols . '">' .
            get_string('criteriagroup', 'completion') . '</th>';

    $current_group = false;
    $col_count = 0;
    //****print_r($criteria);

//Array ( [0] => completion_criteria_activity Object ( [criteriatype] => 4 [table] => course_completion_criteria [required_fields] => Array ( [0] => id [1] => course [2] => criteriatype [3] => module [4] => moduleinstance [5] => courseinstance [6] => enrolperiod [7] => timeend [8] => gradepass [9] => role ) [course] => 6 [module] => resource [moduleinstance] => 103 [enrolperiod] => [date] => [gradepass] => [role] => [optional_fields] => Array ( ) [unique_fields] => Array ( ) [id] => 1 [courseinstance] => [timeend] => ) )





    // for ($i = 0; $i <= count($criteria); $i++) {

    //     if (isset($criteria[$i])) {
    //         $criterion = $criteria[$i];

    //         if ($current_group && $criterion->criteriatype === $current_group->criteriatype) {
    //             ++$col_count;
    //             continue;
    //         }
    //     }

    //     // Print header cell
    //     if ($col_count) {
    //         print '<th scope="col" colspan="'.$col_count.'" class="colheader criteriagroup">'.$current_group->get_type_title().'</th>';
    //     }

    //     if (isset($criteria[$i])) {
    //         // Move to next criteria type
    //         $current_group = $criterion;
    //         $col_count = 1;
    //     }
    // }
//****print_r($criterion);

//completion_criteria_activity Object ( [criteriatype] => 4 [table] => course_completion_criteria [required_fields] => Array ( [0] => id [1] => course [2] => criteriatype [3] => module [4] => moduleinstance [5] => courseinstance [6] => enrolperiod [7] => timeend [8] => gradepass [9] => role ) [course] => 6 [module] => resource [moduleinstance] => 103 [enrolperiod] => [date] => [gradepass] => [role] => [optional_fields] => Array ( ) [unique_fields] => Array ( ) [id] => 1 [courseinstance] => [timeend] => )

    // Overall course completion status
    $colcount2 = 0;
    foreach ($courses_to_include as $unnecessary) {
    //$value = $value * 2;
    print '<th style="text-align: center;">'.get_string('course').'</th>';
    $colcount2++;
    }

    //print '<th>Total</th>';
    print '</tr>';

    // Print aggregation methods
    print PHP_EOL.'<tr style="vertical-align: top">';
    echo '<th scope="row" class="rowheader" colspan="' . $leftcols . '">' .
            get_string('aggregationmethod', 'completion').'</th>';

    $current_group = false;
    $col_count = 0;
    for ($i = 0; $i <= count($criteria); $i++) {

        if (isset($criteria[$i])) {
            $criterion = $criteria[$i];

            if ($current_group && $criterion->criteriatype === $current_group->criteriatype) {
                ++$col_count;
                continue;
            }
        }

        // Print header cell
        if ($col_count) {
            $has_agg = array(
                COMPLETION_CRITERIA_TYPE_COURSE,
                COMPLETION_CRITERIA_TYPE_ACTIVITY,
                COMPLETION_CRITERIA_TYPE_ROLE,
            );

            if (in_array($current_group->criteriatype, $has_agg)) {
                // Try load a aggregation method
                $method = $completion->get_aggregation_method($current_group->criteriatype);

                $method = $method == 1 ? get_string('all') : get_string('any');

            } else {
                $method = '-';
            }

            print '<th scope="col" colspan="'.$colcount2.'" class="colheader aggheader">'.$method.'</th>';
        }
        //print '<th></th>';
        if (isset($criteria[$i])) {
            // Move to next criteria type
            $current_group = $criterion;
            $col_count = 1;
        }
    }

    // Overall course aggregation method
    //print '<th scope="col" class="colheader aggheader aggcriteriacourse">';

    // Get course aggregation
    $method = $completion->get_aggregation_method();

    print $method == 1 ? get_string('all') : get_string('any');
    print '</th>';

    print '</tr>';

    // Print criteria titles
    if (COMPLETION_REPORT_COL_TITLES) {

        print PHP_EOL.'<tr>';
        echo '<th scope="row" class="rowheader" colspan="' . $leftcols . '">' .
                get_string('criteria', 'completion') . '</th>';

        // foreach ($criteria as $criterion) {
        //     // Get criteria details
        //     $details = $criterion->get_title_detailed();
        //     print '<th scope="col" class="colheader criterianame">';
        //     //****this is the activity name
        //     print '<span class="completion-criterianame">'.$details.'</span>';
        //     print '</th>';
        // }
        $coursenames = array_values($courses_to_include);
        // Overall course completion status
        foreach($coursenames as $coursename){
                    print '<th scope="col" class="colheader criterianame">';
        print '<span class="completion-criterianame">'.$coursename.'</span>';
        print '</th>';
        }
        print'<th>Total Courses Complete</th>';
        print '</tr>';
    }

    // Print user heading and icons
    print '<tr>';

    // User heading / sort option
    print '<th scope="col" class="completion-sortchoice" style="clear: both;">';

    $sistring = "&amp;silast={$silast}&amp;sifirst={$sifirst}";

    if ($firstnamesort) {
        print
            get_string('firstname')." / <a href=\"./?course={$course->id}{$sistring}\">".
            get_string('lastname').'</a>';
    } else {
        print "<a href=\"./?course={$course->id}&amp;sort=firstname{$sistring}\">".
            get_string('firstname').'</a> / '.
            get_string('lastname');
    }
    print '</th>';

    // Print user identity columns
    foreach ($extrafields as $field) {
        echo '<th scope="col" class="completion-identifyfield">' .
                get_user_field_name($field) . '</th>';
    }

    ///
    /// Print criteria icons
    ///
    // foreach ($criteria as $criterion) {

    //     // Generate icon details
    //     $iconlink = '';
    //     $iconalt = ''; // Required
    //     $iconattributes = array('class' => 'icon');
    //     switch ($criterion->criteriatype) {

    //         case COMPLETION_CRITERIA_TYPE_ACTIVITY:

    //             // Display icon
    //             $iconlink = $CFG->wwwroot.'/mod/'.$criterion->module.'/view.php?id='.$criterion->moduleinstance;
    //             $iconattributes['title'] = $modinfo->cms[$criterion->moduleinstance]->get_formatted_name();
    //             $iconalt = get_string('modulename', $criterion->module);
    //             break;

    //         case COMPLETION_CRITERIA_TYPE_COURSE:
    //             // Load course
    //             $crs = $DB->get_record('course', array('id' => $criterion->courseinstance));

    //             // Display icon
    //             $iconlink = $CFG->wwwroot.'/course/view.php?id='.$criterion->courseinstance;
    //             $iconattributes['title'] = format_string($crs->fullname, true, array('context' => context_course::instance($crs->id, MUST_EXIST)));
    //             $iconalt = format_string($crs->shortname, true, array('context' => context_course::instance($crs->id)));
    //             break;

    //         case COMPLETION_CRITERIA_TYPE_ROLE:
    //             // Load role
    //             $role = $DB->get_record('role', array('id' => $criterion->role));

    //             // Display icon
    //             $iconalt = $role->name;
    //             break;
    //     }

    //     // Create icon alt if not supplied
    //     if (!$iconalt) {
    //         $iconalt = $criterion->get_title();
    //     }

    //     // Print icon and cell
    //     print '<th class="criteriaicon">';

    //     print ($iconlink ? '<a href="'.$iconlink.'" title="'.$iconattributes['title'].'">' : '');
    //     print $OUTPUT->render($criterion->get_icon($iconalt, $iconattributes));
    //     print ($iconlink ? '</a>' : '');

    //     print '</th>';
    // }

    // Overall course completion status
    foreach ($courses_to_include as $unnecessary){
    print '<th class="criteriaicon">';
    print '<img src="'.$OUTPUT->pix_url('i/course').'" class="icon" alt="'.get_string('course').'" title="'.get_string('coursecomplete', 'completion').'" />';
    print '</th>';
    }
    print '<th></th>';
    print '</tr></thead>';

    echo '<tbody>';
} else {
    // The CSV headers
    $row = array();

    $row[] = "Student ID";
    $row[] = "Student Name";
    $row[] = "Student Email";

    // foreach ($extrafields as $field) {
    //    $row[] = get_user_field_name($field);
    // }

    // Add activity headers
    // foreach ($criteria as $criterion) {

    //     // Handle activity completion differently
    //     if ($criterion->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {

    //         // Load activity
    //         $mod = $criterion->get_mod_instance();
    //         $row[] = $formattedname = format_string($mod->name, true,
    //                 array('context' => context_module::instance($criterion->moduleinstance)));
    //         $row[] = $formattedname . ' - ' . get_string('completiondate', 'report_completion');
    //     }
    //     else {
    //         // Handle all other criteria
    //         $row[] = strip_tags($criterion->get_title_detailed());
    //     }
    // }
    $coursenames = array_values($courses_to_include);
    foreach($coursenames as $coursename){
        $row[] = $coursename;
    }
    $row[] = 'Total Courses Complete';

    $export->add_data($row);
}

///
/// Display a row for each user
///
$total_user_count = 0;
foreach ($progress as $user) {
    $total_user_count++;
    $courses_complete = 0;
    // User name
    if ($csv) {
        $row = array();
        $row[] = $user->id;
        $row[] = fullname($user);
        foreach ($extrafields as $field) {
            $row[] = $user->{$field};
        }
    } else {
        print PHP_EOL.'<tr id="user-'.$user->id.'">';

        if (completion_can_view_data($user->id, $course)) {
            $userurl = new moodle_url('/blocks/completionstatus/details.php', array('course' => $course->id, 'user' => $user->id));
        } else {
            $userurl = new moodle_url('/user/view.php', array('id' => $user->id, 'course' => $course->id));
        }

        print '<th scope="row"><a href="'.$userurl->out().'">'.fullname($user).'</a></th>';
        foreach ($extrafields as $field) {
            echo '<td>'.s($user->{$field}).'</td>';
        }
    }

    // Progress for each course completion criteria
    // foreach ($criteria as $criterion) {

    //     $criteria_completion = $completion->get_user_completion($user->id, $criterion);
    //     $is_complete = $criteria_completion->is_complete();

    //     // Handle activity completion differently
    //     if ($criterion->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {

    //         // Load activity
    //         $activity = $modinfo->cms[$criterion->moduleinstance];

    //         // Get progress information and state
    //         if (array_key_exists($activity->id, $user->progress)) {
    //             $state = $user->progress[$activity->id]->completionstate;
    //         } else if ($is_complete) {
    //             $state = COMPLETION_COMPLETE;
    //         } else {
    //             $state = COMPLETION_INCOMPLETE;
    //         }
    //         if ($is_complete) {
    //             $date = userdate($criteria_completion->timecompleted, get_string('strftimedatetimeshort', 'langconfig'));
    //         } else {
    //             $date = '';
    //         }

    //         // Work out how it corresponds to an icon
    //         switch($state) {
    //             case COMPLETION_INCOMPLETE    : $completiontype = 'n';    break;
    //             case COMPLETION_COMPLETE      : $completiontype = 'y';    break;
    //             case COMPLETION_COMPLETE_PASS : $completiontype = 'pass'; break;
    //             case COMPLETION_COMPLETE_FAIL : $completiontype = 'fail'; break;
    //         }

    //         $auto = $activity->completion == COMPLETION_TRACKING_AUTOMATIC;
    //         $completionicon = 'completion-'.($auto ? 'auto' : 'manual').'-'.$completiontype;

    //         $describe = get_string('completion-'.$completiontype, 'completion');
    //         $a = new StdClass();
    //         $a->state     = $describe;
    //         $a->date      = $date;
    //         $a->user      = fullname($user);
    //         $a->activity  = $activity->get_formatted_name();
    //         $fulldescribe = get_string('progress-title', 'completion', $a);

    //         if ($csv) {
    //             $row[] = $describe;
    //             $row[] = $date;
    //         } else {
    //             print '<td class="completion-progresscell">';

    //             print '<img src="'.$OUTPUT->pix_url('i/'.$completionicon).
    //                   '" alt="'.s($describe).'" class="icon" title="'.s($fulldescribe).'" />';

    //             print '</td>';
    //         }

    //         continue;
    //     }

    //     // Handle all other criteria
    //     $completiontype = $is_complete ? 'y' : 'n';
    //     $completionicon = 'completion-auto-'.$completiontype;

    //     $describe = get_string('completion-'.$completiontype, 'completion');

    //     $a = new stdClass();
    //     $a->state    = $describe;

    //     if ($is_complete) {
    //         $a->date = userdate($criteria_completion->timecompleted, get_string('strftimedatetimeshort', 'langconfig'));
    //     } else {
    //         $a->date = '';
    //     }

    //     $a->user     = fullname($user);
    //     $a->activity = strip_tags($criterion->get_title());
    //     $fulldescribe = get_string('progress-title', 'completion', $a);

    //     if ($csv) {
    //         $row[] = $a->date;
    //     } else {

    //         print '<td class="completion-progresscell">';

    //         if ($allow_marking_criteria === $criterion->id) {
    //             $describe = get_string('completion-'.$completiontype, 'completion');

    //             $toggleurl = new moodle_url(
    //                 '/course/togglecompletion.php',
    //                 array(
    //                     'user' => $user->id,
    //                     'course' => $course->id,
    //                     'rolec' => $allow_marking_criteria,
    //                     'sesskey' => sesskey()
    //                 )
    //             );

    //             print '<a href="'.$toggleurl->out().'" title="'.s(get_string('clicktomarkusercomplete', 'report_completion')).'">' .
    //                 '<img src="'.$OUTPUT->pix_url('i/completion-manual-'.($is_complete ? 'y' : 'n')).
    //                 '" alt="'.s($describe).'" class="icon" /></a></td>';
    //         } else {
    //             print '<img src="'.$OUTPUT->pix_url('i/'.$completionicon).'" alt="'.s($describe).
    //                     '" class="icon" title="'.s($fulldescribe).'" /></td>';
    //         }

    //         print '</td>';
    //     }
    // }

    // Handle overall course completion

    // Load course completion


    foreach($courses_to_include as $key => $value){
        //print $key;
        $params = array(
            'userid'    => $user->id,
            'course'    => $key //$key;
        );

        

        $ccompletion = new completion_completion($params);
        $completiontype =  $ccompletion->is_complete() ? 'y' : 'n';

        $describe = get_string('completion-'.$completiontype, 'completion');

        $a = new StdClass;

        if ($ccompletion->is_complete()) {
            $a->date = userdate($ccompletion->timecompleted, get_string('strftimedatetimeshort', 'langconfig'));
        $courses_complete++;
        $course_totals[$key]++;
        } else {
            $a->date = '';
        }

        $a->state    = $describe;
        $a->user     = fullname($user);
        $a->activity = strip_tags(get_string('coursecomplete', 'completion'));
        $fulldescribe = get_string('progress-title', 'completion', $a);

        if ($csv) {
            $row[] = $a->date;
        } else {

            print '<td class="completion-progresscell">';

            // Display course completion status icon
            //print '<img src="'.$OUTPUT->pix_url('i/completion-auto-'.$completiontype).
            //       '" alt="'.s($describe).'" class="icon" title="'.s($fulldescribe).'" />';
            print $a->date;
            //print $key;
            print '</td>';
        }
    



    }
        if ($csv) {
            $row[] = $courses_complete;
            $export->add_data($row);
        } else {
            print '<td>'.$courses_complete.'</td>';
            print '</tr>';
        }



}

if ($csv) {
    $row = array();
    $row[] = "";
    $row[] = "";
    $row[] = "Percent Complete";

    foreach($course_totals as $total_num){
        //$size = count($course_totals);
        $percent = $total_num / $total_user_count;
        $percent = $percent*100;
        $row[] = $percent;
    }


    $export->add_data($row);
    $export->download_file();
} else {
    //print_r($course_totals);
    print '<tr><td></td><td>Percent Complete</td>';
    foreach($course_totals as $total_num){
        //$size = count($course_totals);
        $percent = $total_num / $total_user_count;
        $percent = $percent*100;
        print '<td>'.$percent.'</td>';
    }
    print '<td></td>';
    print '</tr>';



    echo '</tbody>';
}

print '</table>';
print $pagingbar;

$csvurl = new moodle_url('/report/completion/custom.php', array('course' => $course->id, 'format' => 'csv'));
$excelurl = new moodle_url('/report/completion/custom.php', array('course' => $course->id, 'format' => 'excelcsv'));

print '<ul class="export-actions">';
print '<li><a href="'.$csvurl->out().'">'.get_string('csvdownload','completion').'</a></li>';
print '<li><a href="'.$excelurl->out().'">'.get_string('excelcsvdownload','completion').'</a></li>';
print '</ul>';

echo $OUTPUT->footer($course);

// Trigger a report viewed event.
$event = \report_completion\event\report_viewed::create(array('context' => $context));
$event->trigger();
